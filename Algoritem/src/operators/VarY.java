package operators;

import ia_math.RealInterval;

public class VarY extends Operator {

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		return y;
	}

}
