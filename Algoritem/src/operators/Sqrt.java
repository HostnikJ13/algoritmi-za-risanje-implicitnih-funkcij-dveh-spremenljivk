package operators;

import ia_math.IAMath;
import ia_math.RealInterval;

public class Sqrt extends Operator {
	Operator arg;
	
	public Sqrt(Operator arg) {
		this.arg = arg;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r = arg.eval(x, y);
		try {
			return IAMath.integerRoot(r, new RealInterval(2.0));
		}
		catch (ia_math.IAException e) {
			return new RealInterval();
		}
	}

}
