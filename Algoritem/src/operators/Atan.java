package operators;

import ia_math.IAMath;
import ia_math.RealInterval;

public class Atan extends Operator {
	Operator arg;
	
	public Atan(Operator arg) {
		this.arg = arg;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r = arg.eval(x, y);
		return IAMath.atan(r);
	}


}
