package operators;

import ia_math.RealInterval;

public class Min extends Operator {
	Operator arg1, arg2;
	
	public Min(Operator arg1, Operator arg2) {
		this.arg1 = arg1;
		this.arg2 = arg2;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r1 = arg1.eval(x, y);
		RealInterval r2 = arg2.eval(x, y);
		return new RealInterval(Math.min(r1.lo(), r2.lo()), Math.min(r1.hi(), r2.hi()));
	}

}
