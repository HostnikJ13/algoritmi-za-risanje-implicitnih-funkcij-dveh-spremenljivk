package operators;

import ia_math.IAMath;
import ia_math.RealInterval;

public class Plus extends Operator {
	Operator arg1, arg2;
	
	public Plus(Operator arg1, Operator arg2) {
		this.arg1 = arg1;
		this.arg2 = arg2;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r1 = arg1.eval(x, y);
		RealInterval r2 = arg2.eval(x, y);
		return IAMath.add(r1, r2);
	}

}
