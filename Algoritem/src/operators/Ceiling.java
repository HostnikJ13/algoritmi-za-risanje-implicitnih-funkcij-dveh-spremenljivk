package operators;

import ia_math.RealInterval;

public class Ceiling extends Operator {
	Operator arg;
	
	public Ceiling(Operator arg) {
		this.arg = arg;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r = arg.eval(x, y);
		return new RealInterval(Math.ceil(r.lo()), Math.ceil(r.hi()));
	}

}
