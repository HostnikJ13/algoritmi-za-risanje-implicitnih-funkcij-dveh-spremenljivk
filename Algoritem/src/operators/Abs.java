package operators;

import ia_math.RealInterval;

public class Abs extends Operator {
	Operator arg;
	
	public Abs(Operator arg) {
		this.arg = arg;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r = arg.eval(x, y);
		double l = r.lo();
		double h = r.hi();
		if (l <= 0 && h >= 0) {
			return new RealInterval(0.0, Math.max(Math.abs(l), Math.abs(h)));
		}
		else {
			return new RealInterval(Math.min(Math.abs(l), Math.abs(h)), Math.max(Math.abs(l), Math.abs(h)));
		}
	}

}
