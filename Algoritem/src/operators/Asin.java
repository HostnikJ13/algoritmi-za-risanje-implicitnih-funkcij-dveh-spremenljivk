package operators;

import ia_math.IAMath;
import ia_math.RealInterval;

public class Asin extends Operator {
	Operator arg;
	
	public Asin(Operator arg) {
		this.arg = arg;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r = arg.eval(x, y);
		return IAMath.asin(r);
	}


}
