package operators;

import ia_math.IAMath;
import ia_math.RealInterval;

public class Cos extends Operator {
	Operator arg;
	
	public Cos(Operator arg) {
		this.arg = arg;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r = arg.eval(x, y);
		return IAMath.cos(r);
	}

}
