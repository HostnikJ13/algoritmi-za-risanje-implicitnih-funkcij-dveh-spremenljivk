package operators;

import ia_math.IAMath;
import ia_math.RealInterval;

public class Tan extends Operator {
	Operator arg;
	
	public Tan(Operator arg) {
		this.arg = arg;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r = arg.eval(x, y);
		return IAMath.tan(r);
	}

}
