package operators;

import ia_math.RealInterval;

public class Number extends Operator {
	RealInterval c;
	
	public Number(double c) {
		this.c = new RealInterval(c);
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		return c;
	}

}
