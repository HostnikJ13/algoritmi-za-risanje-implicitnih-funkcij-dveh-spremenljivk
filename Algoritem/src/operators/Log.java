package operators;

import ia_math.IAMath;
import ia_math.RealInterval;

public class Log extends Operator {
	Operator arg;
	
	public Log(Operator arg) {
		this.arg = arg;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r = arg.eval(x, y);
		return IAMath.log(r);
//		try {
//			return IAMath.log(r);
//		}
//		catch (ia_math.IAException e) {
//			return new RealInterval();
//		}
	}


}
