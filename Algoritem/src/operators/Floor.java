package operators;

import ia_math.RealInterval;

public class Floor extends Operator {
	Operator arg;
	
	public Floor(Operator arg) {
		this.arg = arg;
	}

	@Override
	public RealInterval eval(RealInterval x, RealInterval y) {
		RealInterval r = arg.eval(x, y);
		return new RealInterval(Math.floor(r.lo()), Math.floor(r.hi()));
	}

}
