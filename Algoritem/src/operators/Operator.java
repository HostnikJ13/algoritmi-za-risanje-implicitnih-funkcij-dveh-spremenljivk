package operators;

import ia_math.RealInterval;
import operators.Operator;

public abstract class Operator {
	
	public Operator() {
		super();
	}
	
	public static Operator build(String type, Operator arg1, Operator arg2) {
		if (type.equals("Plus")) {
			return new Plus(arg1, arg2);
		}
		else if (type.equals("Minus")) {
			return new Minus(arg1, arg2);
		}
		else if (type.equals("Mul")) {
			return new Mul(arg1, arg2);
		}
		else if (type.equals("Div")) {
			return new Div(arg1, arg2);
		}
		else if (type.equals("Pow")) {
			return new Pow(arg1, arg2);
		}
		else if (type.equals("max")) {
			return new Max(arg1, arg2);
		}
		else if (type.equals("min")) {
			return new Min(arg1, arg2);
		}
		else {
			throw new RuntimeException("Wrong type argument: " + type);
		}
	}
	
	public static Operator build(String type, Operator arg) {
		if (type.equals("Minus")) {
			return new Minus(arg);
		}
		else if (type.equals("abs")) {
			return new Abs(arg);
		}
		else if (type.equals("sqrt")) {
			return new Sqrt(arg);
		}
		else if (type.equals("sin")) {
			return new Sin(arg);
		}
		if (type.equals("cos")) {
			return new Cos(arg);
		}
		if (type.equals("tan")) {
			return new Tan(arg);
		}
		if (type.equals("asin")) {
			return new Asin(arg);
		}
		if (type.equals("acos")) {
			return new Acos(arg);
		}
		if (type.equals("atan")) {
			return new Atan(arg);
		}
		if (type.equals("log")) {
			return new Log(arg);
		}
		if (type.equals("floor")) {
			return new Floor(arg);
		}
		if (type.equals("ceiling")) {
			return new Ceiling(arg);
		}
		else {
			throw new RuntimeException("Wrong type argument: " + type);
		}
	}
	
	public static Operator build(String type) {
		if (type.equals("VarX")) {
			return new VarX();
		}
		else if (type.equals("VarY")) {
			return new VarY();
		}
		else {
			throw new RuntimeException("Wrong type argument: " + type);
		}
	}
	
	public static Operator build(String type, double c) {
		if (type.equals("Num")) {
			return new Number(c);
		}
		else {
			throw new RuntimeException("Wrong type argument: " + type);
		}
	}

	public abstract RealInterval eval(RealInterval x, RealInterval y);
	
//	public abstract Operator build();
	
}
