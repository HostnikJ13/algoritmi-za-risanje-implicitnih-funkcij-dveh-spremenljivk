import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import operators.Operator;

@SuppressWarnings("serial")
public class GlavnoOkno extends JFrame implements ActionListener {
	private Graf slika;
	private JLabel napisLevo;
	private JFormattedTextField vnosLevo;
	private JLabel napisDesno;
	private JFormattedTextField vnosDesno;
	private JLabel napisSpodaj;
	private JFormattedTextField vnosSpodaj;
	private JLabel napisZgoraj;
	private JFormattedTextField vnosZgoraj;
	private JButton gumbRisi;
	private JLabel napisFormula;
	private JTextField vnosFormula;
	private JButton gumbShrani;
	private FileDialog fd;
	
	public GlavnoOkno() {
		super();
		
		this.getContentPane().setBackground(new Color(235, 255, 255));
		
		NumberFormat format = NumberFormat.getNumberInstance();
		format.setMaximumFractionDigits(50);
		format.setGroupingUsed(false);
		
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Double.class);
		
		setTitle("Ri�emo graf");
		slika = new Graf(768, 768);
		
		this.setLayout(new GridBagLayout());
		
		// slika
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 2;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.CENTER;
		add(slika, c);
		
		c = new GridBagConstraints();
		c.gridx = 5;
		c.gridy = 3;
//		c.weighty = (1/10);
//		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.CENTER;
		gumbRisi = new JButton("nari�i");
		gumbRisi.addActionListener(this);
		add(gumbRisi, c);
		
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
//		c.weightx = 0.1;
		c.anchor = GridBagConstraints.EAST;
		napisFormula = new JLabel("formula:  ");
		add(napisFormula, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 0.1;
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth = GridBagConstraints.REMAINDER;
		vnosFormula = new JTextField();
		vnosFormula.setColumns(50);
		vnosFormula.addActionListener(this);
		add(vnosFormula, c);
		
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.125;
//		c.anchor = GridBagConstraints.EAST;
		napisLevo = new JLabel("levo:  ");
		add(napisLevo, c);
		
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.125;
//		c.anchor = GridBagConstraints.WEST;
		vnosLevo = new JFormattedTextField(formatter);
		vnosLevo.setValue(-10.0);
		vnosLevo.setColumns(5);
		add(vnosLevo, c);
		
		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 0.125;
//		c.anchor = GridBagConstraints.EAST;
		napisDesno = new JLabel("desno:  ");
		add(napisDesno, c);
		
		c = new GridBagConstraints();
		c.gridx = 3;
		c.gridy = 1;
		c.weightx = 0.125;
//		c.anchor = GridBagConstraints.WEST;
		vnosDesno = new JFormattedTextField(formatter);
		vnosDesno.setValue(10.0);
		vnosDesno.setColumns(5);
		add(vnosDesno, c);
		
		c = new GridBagConstraints();
		c.gridx = 4;
		c.gridy = 1;
		c.weightx = 0.125;
//		c.anchor = GridBagConstraints.EAST;
		napisSpodaj = new JLabel("spodaj:  ");
		add(napisSpodaj, c);
		
		c = new GridBagConstraints();
		c.gridx = 5;
		c.gridy = 1;
		c.weightx = 0.125;
//		c.anchor = GridBagConstraints.WEST;
		vnosSpodaj = new JFormattedTextField(formatter);
		vnosSpodaj.setValue(-10.0);
		vnosSpodaj.setColumns(5);
		add(vnosSpodaj, c);
		
		c = new GridBagConstraints();
		c.gridx = 6;
		c.gridy = 1;
		c.weightx = 0.125;
//		c.anchor = GridBagConstraints.EAST;
		napisZgoraj = new JLabel("zgoraj:  ");
		add(napisZgoraj, c);
		
		c = new GridBagConstraints();
		c.gridx = 7;
		c.gridy = 1;
		c.weightx = 0.125;
//		c.anchor = GridBagConstraints.WEST;
		vnosZgoraj = new JFormattedTextField(formatter);
		vnosZgoraj.setValue(10.0);
		vnosZgoraj.setColumns(5);
		add(vnosZgoraj, c);
		
		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 3;
//		c.weightx = 1;
//		c.weighty = (1/10);
//		c.anchor = GridBagConstraints.WEST;
		gumbShrani = new JButton("shrani");
		gumbShrani.addActionListener(this); 
		add(gumbShrani, c);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// risanje
		if (e.getSource() == gumbRisi) {
			double l = ((Number) vnosLevo.getValue()).doubleValue();
			double d = ((Number) vnosDesno.getValue()).doubleValue();
			double s = ((Number) vnosSpodaj.getValue()).doubleValue();
			double z = ((Number) vnosZgoraj.getValue()).doubleValue();
			
			String formula = vnosFormula.getText();
			
			ANTLRInputStream input = new ANTLRInputStream(formula);
			RelationLexer lexer = new RelationLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			RelationParser parser = new RelationParser(tokens);
			ParseTree tree = parser.relation();
			RelationBaseVisitorImpl visitor = new RelationBaseVisitorImpl();
			System.out.println(tree.toStringTree(parser));
			
			Operator op = visitor.visit(tree);
			String relOpType = visitor.relOp;
			slika.narisi(l, d, s, z, op, relOpType);
			} 
		
		else if (e.getSource() == gumbShrani){
			fd = new FileDialog(new Frame(), "Save", FileDialog.SAVE);
			fd.setFile(".png");
			fd.setVisible(true);
			String ime = fd.getFile();
			if (ime != null){
				File file = new File(fd.getDirectory(), fd.getFile());
				try {
					ImageIO.write(slika.slika, "png", file);
				} 
				catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
}
