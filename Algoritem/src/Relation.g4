grammar Relation;

relation
   : addingExpression relop addingExpression
   ;

addingExpression
   : addingExpression PLUS multiplyingExpression	# Addition
   | addingExpression MINUS multiplyingExpression	# Subtraction
   | multiplyingExpression							# SingleMul
   ;

multiplyingExpression
   : multiplyingExpression MUL powExpression	# Multiplication
   | multiplyingExpression DIV powExpression	# Division
   | MINUS powExpression						# ChangeSign1
   | powExpression								# SinglePow
   ;

powExpression
   : unaryMinus POW powExpression	# Pow
   | unaryMinus						# SingleUnaryMinus
   ;
   
unaryMinus
   : MINUS atom		# ChangeSign2
   | atom			# SingleAtom
   ;

atom
   : number							# Num
   | variable						# Var
   | LPAREN addingExpression RPAREN	# Braces
   | function						# Func
   | fun2var						# F2v
   | PI								# ConstantPi
   | E								# ConstantE
   ;

function
   : functionName LPAREN addingExpression RPAREN
   ;
   
fun2var
   : functionName LPAREN addingExpression COMMA addingExpression RPAREN
   ;

functionName
   : SQRT
   | ABS
   | MAX
   | MIN
   | COS
   | TAN
   | SIN
   | ACOS
   | ATAN
   | ASIN
   | LOG
   | LN
   | FLOOR
   | CEILING
   ;

relop
   : EQ
   | GT
   | LT
   | LEQ
   | GEQ
   ;

number
   : MINUS? DIGIT + (POINT DIGIT +)?
   ;

variable
   : MINUS? X	# VarX
   | MINUS? Y	# VarY
   ;

SQRT
   : 'sqrt'
   ;
   
ABS
   : 'abs'
   ;
   
MAX
   : 'max'
   ;
   
MIN
   : 'min'
   ;         

COS
   : 'cos'
   ;

SIN
   : 'sin'
   ;

TAN
   : 'tan'
   ;

ACOS
   : 'acos'
   ;

ASIN
   : 'asin'
   ;

ATAN
   : 'atan'
   ;

LN
   : 'ln'
   ;

LOG
   : 'log'
   ;

FLOOR
   : 'floor'
   ;
   
CEILING
   : 'ceiling'
   ;

LPAREN
   : '('
   ;

RPAREN
   : ')'
   ;

PLUS
   : '+'
   ;

MINUS
   : '-'
   ;

MUL
   : '*'
   ;

DIV
   : '/'
   ;

GT
   : '>'
   ;

LT
   : '<'
   ;

EQ
   : '='
   ;
   
LEQ
   : '<='
   ;
   
GEQ
   : '>='
   ;

POINT
   : '.'
   ;
   
COMMA
   : ','
   ;

E
   : 'e' | 'E'
   ;
   
PI
   : 'pi'
   ;

POW
   : '^'
   ;

X
   : 'x'
   ;
   
Y
   : 'y'
   ;

LETTER
   : ('a' .. 'z') | ('A' .. 'Z')
   ;

DIGIT
   : ('0' .. '9')
   ;

WS
   : [ \r\n\t] + -> channel (HIDDEN)
   ;