import operators.Operator;

public class RelationBaseVisitorImpl extends RelationBaseVisitor<Operator> {
	String relOp;
	
	@Override
	public Operator visitRelation(RelationParser.RelationContext ctx) {
		visit(ctx.relop());
		return Operator.build("Minus", visit(ctx.addingExpression(0)), visit(ctx.addingExpression(1)));
	}
	
	@Override
	public Operator visitAddition(RelationParser.AdditionContext ctx) {
		return Operator.build("Plus", visit(ctx.addingExpression()), visit(ctx.multiplyingExpression()));
	}
	
	@Override
	public Operator visitSubtraction(RelationParser.SubtractionContext ctx) {
		return Operator.build("Minus", visit(ctx.addingExpression()), visit(ctx.multiplyingExpression()));
	}
	
	@Override
	public Operator visitMultiplication(RelationParser.MultiplicationContext ctx) {
		return Operator.build("Mul", visit(ctx.multiplyingExpression()), visit(ctx.powExpression()));
	}
	
	@Override
	public Operator visitDivision(RelationParser.DivisionContext ctx) {
		return Operator.build("Div", visit(ctx.multiplyingExpression()), visit(ctx.powExpression()));
	}
	
	@Override
	public Operator visitChangeSign1(RelationParser.ChangeSign1Context ctx) {
		return Operator.build("Minus", visit(ctx.powExpression()));
	}
	
	@Override
	public Operator visitPow(RelationParser.PowContext ctx) {
		return Operator.build("Pow", visit(ctx.unaryMinus()), visit(ctx.powExpression()));
	}
	
	@Override
	public Operator visitChangeSign2(RelationParser.ChangeSign2Context ctx) {
		return Operator.build("Minus", visit(ctx.atom()));
	}
	
	@Override
	public Operator visitNum(RelationParser.NumContext ctx) {
		return Operator.build("Num", Double.valueOf(ctx.getText()));
	}
	
	@Override
	public Operator visitBraces(RelationParser.BracesContext ctx) {
		return visit(ctx.addingExpression());
	}
	
	@Override
	public Operator visitConstantPi(RelationParser.ConstantPiContext ctx) {
		return Operator.build("Num", Math.PI);
	}
	
	@Override
	public Operator visitConstantE(RelationParser.ConstantEContext ctx) {
		return Operator.build("Num", Math.E);
	}
	
	@Override
	public Operator visitFunction(RelationParser.FunctionContext ctx) {
		return Operator.build(
				ctx.functionName().getText(),
				visit(ctx.addingExpression()));
	}
	
	@Override
	public Operator visitFun2var(RelationParser.Fun2varContext ctx) {
		return Operator.build(
				ctx.functionName().getText(),
				visit(ctx.addingExpression(0)),
				visit(ctx.addingExpression(1)));
	}
	
	@Override
	public Operator visitVarX(RelationParser.VarXContext ctx) {
		return Operator.build("VarX");
	}
	
	@Override
	public Operator visitVarY(RelationParser.VarYContext ctx) {
		return Operator.build("VarY");
	}
	
	@Override
	public Operator visitRelop(RelationParser.RelopContext ctx) {
		relOp = ctx.getText();
		return null;
	}
	
}
