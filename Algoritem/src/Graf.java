import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import auxiliaryFunctions.Gcd;
import auxiliaryFunctions.Region;
import auxiliaryFunctions.RelOp;
import ia_math.RealInterval;
import operators.Operator;

@SuppressWarnings("serial")
public class Graf extends JPanel {
	public BufferedImage slika;
	private Thread slikarThread;
	private boolean ustaviSe;
	int w, h;
	
	public Graf(int width, int height) {
		super();
		slika = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		w = width;
		h = height;
	}
	
	private void pobarvajCrno(int l, int d, int s, int z) {
		for (int y=s; y < z; y += 1) {
			if (ustaviSe) {
				return;
			}
			repaint();
			for (int x=l; x < d; x += 1) {
				slika.setRGB(x, h-1-y, Color.BLACK.getRGB());
			}
		}
	}
	
	private void pobarvajBelo(int l, int d, int s, int z) {
		for (int y=s; y < z; y += 1) {
			if (ustaviSe) {
				return;
			}
			repaint();
			for (int x=l; x < d; x += 1) {
				slika.setRGB(x, h-1-y, Color.WHITE.getRGB());
			}
		}
	}
	
	private void pobarvajRdece(int l, int d, int s, int z) {
		for (int y=s; y < z; y += 1) {
			if (ustaviSe) {
				return;
			}
			repaint();
			for (int x=l; x < d; x += 1) {
				slika.setRGB(x, h-1-y, Color.RED.getRGB());
			}
		}
	}
	
	private List<Region> refinePixels(List<Region> undefined, int k, Operator op, String relOpType) {
		List<Region> newUndefined = new ArrayList<Region>();
		for (Region u : undefined) {
			int rezultat = RelOp.eval(relOpType, op.eval(u.x, u.y), new RealInterval(0.0));
			if (rezultat == 1) {
				pobarvajCrno(u.l, u.r, u.b, u.t);
			}
			else if (rezultat == 2) {
				pobarvajBelo(u.l, u.r, u.b, u.t);
			}
			else {
				newUndefined.add(new Region(
						u.l, u.l+(u.r-u.l)/2, u.b, u.b+(u.t-u.b)/2,
						new RealInterval(u.x.lo(), u.x.lo()+(u.x.hi()-u.x.lo())/2.0),
						new RealInterval(u.y.lo(), u.y.lo()+(u.y.hi()-u.y.lo())/2.0)));
				newUndefined.add(new Region(
						u.l+(u.r-u.l)/2, u.r, u.b, u.b+(u.t-u.b)/2,
						new RealInterval(u.x.lo()+(u.x.hi()-u.x.lo())/2.0, u.x.hi()),
						new RealInterval(u.y.lo(), u.y.lo()+(u.y.hi()-u.y.lo())/2.0)));
				newUndefined.add(new Region(
						u.l, u.l+(u.r-u.l)/2, u.b+(u.t-u.b)/2, u.t,
						new RealInterval(u.x.lo(), u.x.lo()+(u.x.hi()-u.x.lo())/2.0),
						new RealInterval(u.y.lo()+(u.y.hi()-u.y.lo())/2.0, u.y.hi())));
				newUndefined.add(new Region(
						u.l+(u.r-u.l)/2, u.r, u.b+(u.t-u.b)/2, u.t,
						new RealInterval(u.x.lo()+(u.x.hi()-u.x.lo())/2.0, u.x.hi()),
						new RealInterval(u.y.lo()+(u.y.hi()-u.y.lo())/2.0, u.y.hi())));
//				if (k == 0) {
//					pobarvajRdece(u.l, u.r, u.b, u.t);
//				}
			}
		}
		return newUndefined;
	}
	
	private List<Region> refineSubixels(List<Region> undefined, int k, Operator op, String relOpType) {
		List<Region> newUndefined = new ArrayList<Region>();
		List<Region> copyOfUndefined = new ArrayList<>(undefined);
		for (Region u : copyOfUndefined) {
			boolean absent1 = true;
			boolean absent2 = true;
			int rezultat = RelOp.eval(relOpType, op.eval(u.x, u.y), new RealInterval(0.0));
			if (rezultat == 1) {
				pobarvajCrno(u.l, u.r, u.b, u.t);
				undefined.removeIf(uu -> uu.l == u.l && uu.r == u.r && uu.b == u.b && uu.t == u.t);
				newUndefined.removeIf(uu -> uu.l == u.l && uu.r == u.r && uu.b == u.b && uu.t == u.t);
			}
			else if (rezultat == 2) {
				undefined.remove(u);
				for (Region uu : undefined) {
					if (uu.l == u.l && uu.r == u.r && uu.b == u.b && uu.t == u.t) {
						absent1 = false;
						break;
					}
				}
				for (Region uu : newUndefined) {
					if (uu.l == u.l && uu.r == u.r && uu.b == u.b && uu.t == u.t) {
						absent2 = false;
						break;
					}
				}
				if (absent1 && absent2) {
					pobarvajBelo(u.l, u.r, u.b, u.t);
				}
			}
			else {
				newUndefined.add(new Region(
						u.l, u.r, u.b, u.t,
						new RealInterval(u.x.lo(), u.x.lo()+(u.x.hi()-u.x.lo())/2.0),
						new RealInterval(u.y.lo(), u.y.lo()+(u.y.hi()-u.y.lo())/2.0)));
				newUndefined.add(new Region(
						u.l, u.r, u.b, u.t,
						new RealInterval(u.x.lo()+(u.x.hi()-u.x.lo())/2.0, u.x.hi()),
						new RealInterval(u.y.lo(), u.y.lo()+(u.y.hi()-u.y.lo())/2.0)));
				newUndefined.add(new Region(
						u.l, u.r, u.b, u.t,
						new RealInterval(u.x.lo(), u.x.lo()+(u.x.hi()-u.x.lo())/2.0),
						new RealInterval(u.y.lo()+(u.y.hi()-u.y.lo())/2.0, u.y.hi())));
				newUndefined.add(new Region(
						u.l, u.r, u.b, u.t,
						new RealInterval(u.x.lo()+(u.x.hi()-u.x.lo())/2.0, u.x.hi()),
						new RealInterval(u.y.lo()+(u.y.hi()-u.y.lo())/2.0, u.y.hi())));
//				if (k == 0) {
//					pobarvajRdece(u.l, u.r, u.b, u.t);
//				}
			}
		}
		return newUndefined;
	}
	
	private void izracunajSliko(double levo, double desno, double spodaj, double zgoraj, Operator op, String relOpType) {
		int k = Gcd.gcdThing(w, h);
	    k = (int) Math.floor(Math.log(Gcd.gcdThing(k, (int) Math.pow(2.0, Math.floor(Math.log(k)/Math.log(2.0)))))/Math.log(2.0));
	    int dvaNaKa = (int) Math.pow(2, k);
	    
		List<Region> undefined = new ArrayList<Region>();
		for (int a = 0; a*dvaNaKa < w; a += 1) {
			for (int b=0; b*dvaNaKa < h; b += 1) {
				double u = (desno-levo)/(w/dvaNaKa);
				double v = (zgoraj-spodaj)/(h/dvaNaKa);
				undefined.add(
						new Region(
								a*dvaNaKa, (a+1)*dvaNaKa, b*dvaNaKa, (b+1)*dvaNaKa,
								new RealInterval(levo+a*u, levo+(a+1)*u), new RealInterval(spodaj+b*v, spodaj+(b+1)*v)));
			}
		}
		System.out.println(undefined.size());
	    
	    while (k > 0 && !undefined.isEmpty()) {
	    	undefined = refinePixels(undefined, k, op, relOpType);
	    	k -= 1;
	    	System.out.println(undefined.size());
	    }
	    
	    while (!undefined.isEmpty() && k > -3) {
	    	undefined = refineSubixels(undefined, k, op, relOpType);
	    	k -= 1;
	    	System.out.println(undefined.size());
	    }
	    
	    // koordinatni sistem
	    Graphics2D g2d = slika.createGraphics();
	    g2d.setColor(Color.BLACK);
	    int crtica = (w+h)/100;
	    g2d.drawLine(0, h/2, w, h/2);				// x-os
	    g2d.drawLine(w/4, h/2-crtica, w/4, h/2+crtica);
	    g2d.drawLine(3*w/4, h/2-crtica, 3*w/4, h/2+crtica);
	    g2d.drawLine(w/2, 0, w/2, h);				// y-os
	    g2d.drawLine(w/2-crtica, h/4, w/2+crtica, h/4);
	    g2d.drawLine(w/2-crtica, 3*h/4, w/2+crtica, 3*h/4);
//	    g2d.setFont(new Font("Times New Roman", Font.PLAIN, (int) (1.7*crtica))); // cifre
//	    g2d.drawString(String.valueOf(zgoraj), w/2+2, (int) (1.7*crtica));
	    g2d.dispose();
	    
	    System.out.println("konec");
	}

	public void narisi(double levo, double desno, double spodaj, double zgoraj, Operator op, String relOpType) {
		if (slikarThread != null) {
			ustaviSe = true;
			try {
				slikarThread.join();
			} catch (InterruptedException e) {
//				e.printStackTrace();
			}
			slikarThread = null;
		}
		slika = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Runnable slikar = new Runnable() {
			public void run() {
				izracunajSliko(levo, desno, spodaj, zgoraj, op, relOpType);
				slikarThread = null;
				repaint();
			}
		};
		slikarThread = new Thread(slikar);
		ustaviSe = false;
		slikarThread.start();
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(slika.getWidth(), slika.getHeight());
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int w = slika.getWidth();
		int h = slika.getHeight();
		int x = (this.getWidth() - w)/2;
		int y = (this.getHeight() - h)/2;
		g.drawImage(slika, x, y, w, h, Color.RED, null);
	}
}
