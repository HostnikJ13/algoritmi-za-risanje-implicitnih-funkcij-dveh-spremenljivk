package auxiliaryFunctions;

import ia_math.IAException;
import ia_math.RealInterval;

public class Region {

	public int l, r, b, t;
	public RealInterval x, y;
	
	public Region(int left, int right, int bottom, int top, RealInterval x, RealInterval y) throws IAException{
		if (left <= right) {
			l = left;
			r = right;
		}
		else throw new IAException("Region must have: left <= right");
		if (bottom <= top) {
			b = bottom;
			t = top;
		}
		else throw new IAException("Region must have: bottom <= top");
		this.x = x;
		this.y = y;
	}
	
}
