package auxiliaryFunctions;

import ia_math.RealInterval;

/**
 * Class with relational operators for interval arithmetic.
 */
public class RelOp {
	
	public static int eval(String relOpType, RealInterval x, RealInterval y) {
		if (relOpType.equals("=")) {
			return RelOp.equals(x, y);
		}
		else if (relOpType.equals("<=")) {
			return RelOp.leq(x, y);
		}
		else if (relOpType.equals(">=")) {
			return RelOp.geq(x, y);
		}
		else if (relOpType.equals("<")) {
			return RelOp.lessThan(x, y);
		}
		else {
			return RelOp.greaterThan(x, y);
		}
	}
	
	/**
	 * @param x	interval
	 * @param y	interval
	 * @return	1 if x = y, 2 if x < y or x > y or else 3
	 */
	public static int equals(RealInterval x, RealInterval y) {
		if (x.lo() == y.hi() && x.lo() == x.hi() && y.lo() == y.hi()) {
			return 1;
		}
		else if (lessThan(x, y) == 1 || greaterThan(x, y) == 1) {
			return 2;
		}
		else {
			return 3;
		}
	}

	public static int greaterThan(RealInterval x, RealInterval y) {
		if (x.lo() > y.hi() && x.hi() > y.lo()) {
			return 1;
		}
		else if (x.lo() <= y.hi() && x.hi() <= y.lo()) {
			return 2;
		}
		else {
			return 3;
		}
	}
	
	public static int lessThan(RealInterval x, RealInterval y) {
		return greaterThan(y, x);
	}
	
	public static int geq(RealInterval x, RealInterval y) {
		if (x.lo() >= y.hi() && x.hi() >= y.lo()) {
			return 1;
		}
		else if (x.lo() < y.hi() && x.hi() < y.lo()) {
			return 2;
		}
		else {
			return 3;
		}
	}
	
	public static int leq(RealInterval x, RealInterval y) {
		return geq(y, x);
	}
	
}
