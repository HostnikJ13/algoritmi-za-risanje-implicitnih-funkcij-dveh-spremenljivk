package auxiliaryFunctions;

import java.math.BigInteger;

/**
 * Razred za ra�unanje najve�jega skupnega delitelja.
 */
public class Gcd {
	
	/**
	 * @param a - celo �tevilo
	 * @param b - celo �tevilo
	 * @return najve�ji skupni delitelj �tevil a in b
	 */
	public static int gcdThing(int a, int b) {
	    BigInteger b1 = BigInteger.valueOf(a);
	    BigInteger b2 = BigInteger.valueOf(b);
	    BigInteger gcd = b1.gcd(b2);
	    return gcd.intValue();
	}
	
}
