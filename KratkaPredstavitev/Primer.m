x = -10:0.001:10;
f = exp(1).^log(x);
figure
plot(x, f)

f = sqrt(x) ./ sqrt(x-2);
figure
plot(x, f)

f = sqrt(x-9) .* sqrt(-5-x);
figure
plot(x, f)

x = 0:0.001:10;
f = (x.^2 - 4*x)./(x - 4.001);
figure
plot(x, f)

figure
ezplot('((x.^2 - 4.*x)/(x - 4.001))^2 + ((y.^2 - 4.*y)/(y - 4.001))^2=100', [-12 12 -12 12])

x = -3:0.001:2;
f = x.^x;
figure
plot(x, f)

figure
ezplot('abs(1 - x.^2 - y.^2) = 1 - x.^2 - y.^2', [-1.2 1.2 -1.2 1.2])

x = -10:0.001:10;
f = atan(9^(9^9)*(x-3));
figure
plot(x, f)